# OpenML dataset: Daily-Exchange-Rates-per-Euro-1999-2021

https://www.openml.org/d/43799

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

21+ years of the euro  

  04 Jan 1999 - 12 Feb 2021

It wasn't until 1999 that the euro really began its journey, when 11 countries (Austria, Belgium, Finland, France, Germany, Ireland, Italy, Luxembourg, the Netherlands, Portugal and Spain) fixed their exchange rates and created a new currency with monetary policy passed to the European Central Bank. Today euro is 20+ years old.
Content
Reference rates are euro foreign exchange rates observed on major foreign exchange trading venues at a certain point in time = they are the price of one currency in terms of another currency. The rates are usually updated around 16:00 CET on every working day, except on TARGET closing days.   
Dataset contains date and Euro rate corresponding to Australian dollar, Bulgarian lev, Brazilian real, Canadian dollar, Swiss franc, Chinese yuan renminbi, Cypriot pound, Czech koruna, Danish krone, Estonian kroon, UK pound sterling, Greek drachma, Hong Kong dollar, Croatian kuna, Hungarian forint, Indonesian rupiah, Israeli shekel, Indian rupee, Iceland krona, Japanese yen, Korean won, Lithuanian litas, Latvian lats, Maltese lira, Mexican peso, Malaysian ringgit, Norwegian krone, New Zealand dollar, Philippine peso, Polish zloty, Romanian leu, Russian rouble, Swedish krona, Singapore dollar, Slovenian tolar, Slovak koruna, Thai baht, Turkish lira, US dollar, South African rand.

Some currency in the list doesn't exist anymore; it was replaced by the Euro : Cypriot pound (2007), Estonian kroon (2011), Greek drachma (2002), Lithuanian litas (2015), Latvian lats (2014), Maltese lira (2008), Slovenian tolar (2007), Slovak koruna (2009).   
Bulgarian lev since 2002 is pegged to the Euro: 1  = 1.9558 leva.

Acknowledgements
All data provided by European Central Bank Statistical Data WareHouse, EXR - Exchange Rates.  
Dataset is versioned and stays on weekly update.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43799) of an [OpenML dataset](https://www.openml.org/d/43799). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43799/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43799/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43799/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

